import sys
import time
import datetime
import argparse

import numpy as np
np.random.seed(1337) # seed the random number generator to produce the same initialization of the neural network

from keras.models import Sequential, model_from_json
from keras.layers.core import Dense, Dropout
from keras.optimizers import Adagrad

'''
Read corpus from standard input

inputs: 
vocab: a dictionary of words and their associated indices
sent_end_marker: string containing the sentence end marker

outputs:
data_train_x: list of vectors representing the context for a bigram
data_train_y: list of vectors representing the word to be predicted for a bigram
'''
def read_corpora(vocab,sent_end_marker="</s>"):
    train=[]
    corpus_size=0
    context_size=0
    max_context_size = 1 
    bigram_list = []
    voc_size=len(vocab)
    sent_end_idx=vocab[sent_end_marker]
    for line in sys.stdin:
        line=line.rstrip('\n')
        corpus_size += 1
        z=np.zeros(voc_size)
        z[vocab[line]] = 1
        bigram_list.append(z)
        if context_size < max_context_size:
            context_size += 1
        elif (vocab[line] == sent_end_idx):
            context_size = 0
            train.append(list(bigram_list))
            del bigram_list[:]
        else:
            train.append(list(bigram_list))
            bigram_list.pop(0)
            
    data_train= np.array(train)
    data_train=data_train.astype(np.int32)
    data_train_x =data_train[:,0,:]
    data_train_y =data_train[:,-1,:] 
    return data_train_x, data_train_y

'''
Save the model

inputs:
model: object of the model to be saved
model_path: path to file to save the model structure
weights_path: path to file to save the model weights

outputs:
None
'''
def save(model,model_path,weights_path):
    if model_path is None:
        # Generate filename based on date
        date_obj = datetime.datetime.now()
        date_str = date_obj.strftime('%Y-%m-%d-%H:%M:%S')
        model_path = 'ffnn.%s.json' % date_str
    
    if weights_path is None:
        date_obj = datetime.datetime.now()
        date_str = date_obj.strftime('%Y-%m-%d-%H:%M:%S')
        weights_path = 'ffnn.%s.h5' % date_str
    
    json_string = model.to_json()
    f = open(model_path, 'w')
    f.write(json_string)
    f.close
    model.save_weights(weights_path)
    
'''
Compute the perplexity

inputs:
model: object of the model to use to compute the perplexity
x_test: word encoding vectors representing the context of a bigram
y_test: word encoding vectors representing the word to be predicted of a bigram

outputs:
Prints the perplexity on the total number of words on the input data
'''
def ppl(model, x_test, y_test):
    n_test = x_test.shape[0]
    predictions = np.log(model.predict(x_test))[np.arange(n_test),np.where(y_test>0)[1]]
    total_score = np.sum(predictions)
    print "Words: %d, Perplexity %f" % (n_test,np.exp(-1*total_score/n_test))

'''
Build the feedforward neural network

inputs:
input_dim: dimension of the input layer
n_hidden1: dimension of the first hidden layer
n_hidden2: dimension of the second hidden layer
dr: dropout rate to avoid overfitting in the neural network
activation_fn: string to specify the activation function for the hidden layers
output_fn: string to specify the activation function of the output layer

outputs:
model: the model object with the neural network structure
'''
def build(input_dim, n_hidden1,n_hidden2,dr=0.5,activation_fn='tanh',output_fn='softmax'):
    #BEGIN
    #Add your code for build function here
    #END
'''
Fit the model on the data

inputs:
x_train: word encoding vectors representing the context of a bigram
y_train: word encoding vectors representing the word to be predicted of a bigram

outputs:
None
'''
def fit(x_train,y_train,model,lr=0.01,epsilon=1e-06,nb_epochs=100):   
    #BEGIN
    #Add your code for fit function here
    #END

'''
Main function
'''
if __name__ == '__main__':
    #Parse arguments
    parser = argparse.ArgumentParser(description='Run an FFNNLM',usage='ffnnlm [-h] -vocab voc_file -model path_to_model_file -weights path_to_weights_file [-train|-ppl] < data (stdin)')
    parser.add_argument('-model', help="model file path")
    parser.add_argument('-weights', help="weights file path")
    parser.add_argument('-sep', help="specify the sentence end marker, do specify the -vocab flag as well (default: </s>)")
    parser.add_argument('-vocab', help="vocab file")
    parser.add_argument('-train', help="train the FFNN", action="store_true")
    parser.add_argument('-act', help="activation function")
    parser.add_argument('-ppl', help="print the perplexity on the data", action="store_true")
    parser.add_argument('-hidden1', type=int, help="number of nodes in the first hidden layer (default: 500)")
    parser.add_argument('-hidden2', type=int, help="number of nodes in the second hidden layer (default: 500)")
    parser.add_argument('-lr', type=float, help="specify learning rate (default: 0.01)")
    parser.add_argument('-dr', type=float, help="specify drop out rate (default: 0.1)")
    parser.add_argument('-eps', type=float, help="specify epsilon for adagrad (default: 1e-6)")    
    parser.add_argument('-epochs', type=int, help="specify number of epochs of training to run (default: 100)")
    args = parser.parse_args()
    
    t0 = time.time()
    
    #read vocab from file  
    sent_end_idx=1
    vocab={}
    idx=-1
    sent_end_marker="</s>"
    if args.sep:
        sent_end_marker=args.sep

    if args.vocab:
        with open(args.vocab, 'r') as searchfile:
            for line in searchfile:
                idx +=1
                line=line.rstrip("\r\n")
                vocab[line]=idx
    else:
        print >> sys.stderr, "Must specify the vocab file using the -vocab flag"

    #read corpora from stdin 
    x,y = read_corpora(vocab,sent_end_marker=sent_end_marker)
    
    #read model parameters
    n_hidden1=500
    if args.hidden1:
        n_hidden1 = args.hidden1
    
    n_hidden2=500
    if args.hidden2:
        n_hidden2 = args.hidden2
    
    lr=0.01
    if args.lr:
        lr = args.lr
    
    dr=0.1
    if args.dr:
        dr = args.dr
    
    epsilon=0.000001
    if args.eps:
        epsilon = args.eps
    
    nb_epochs = 100
    if args.epochs:
        nb_epochs = args.epochs
    
    act = 'tanh'
    if args.act:
        act = args.act
        
    if args.train: #train the model on data and save the model
        voc_size= len(vocab)        
        model=build(input_dim=voc_size,n_hidden1=n_hidden1,n_hidden2=n_hidden2,activation_fn=act,dr=dr)
        fit(x_train=x, y_train=y,model=model,nb_epochs=nb_epochs)
        save(model=model,model_path=args.model,weights_path=args.weights)
    elif args.ppl: #load the model and calculate perplexity on data 
        try:
            args.model
        except NameError:
            print >> sys.stderr, "Must specify the model file using the -model flag for perplexity calculation"
        try:
            args.weights
        except NameError:
            print >> sys.stderr, "Must specify the weights file using the -weights flag for perplexity calculation"
        
        model = model_from_json(open(args.model).read())
        model.load_weights(args.weights)
        ppl(model,x,y)
    else :
        print >> sys.stderr, "Must specify what to do with the file"        
    
    print >> sys.stderr, "Elapsed time: %f" % (time.time() - t0)